cc1718
=============

Repositorio de la asignatura Códigos y Criptografía, asignatura optativa de cuarto curso del Grado de Ingeniería Informática, de la Universidad de Córdoba. Sólo se versionan las tareas relacionadas con la programación (MATLAB).

