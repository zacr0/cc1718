function [frecuencia, freordenada] = cripto_ana_orden(v)
% Obtiene las frecuencias de las letras de un criptograma v, obtenido 
% mediante cifrado afin.
    
    % TRATAMIENTO DE ENTRADA
    % v debe ser una cadena
    if ~ischar(v)
        error('Error: v debe ser una cadena de caracteres.');
    end
    
    % ANALISIS DE FRECUENCIAS
    % Obtencion de indice numericos de caracteres del texto
    v = letranumero(v);
    
    % Calculo de frecuencia de caracteres
    frecuencia = zeros(1,27); % 27: longitud del abecedario
    for i=1:length(v)
        frecuencia(v(i)+1)= frecuencia(v(i)+1)+1;
    end
    % Frecuencia relativa al tamaño del mensaje
    frecuencia = frecuencia / numel(v);
    
    % Agrega fila con indices de las letras del alfabeto
    for i=1:numel(frecuencia)
        frecuencia(2,i) = i-1;
    end
    
    % Transposicion de matrices (2x27 -> 27x2)
    frecuencia = frecuencia';
    
    % Ordenamiento por frecuencia (descendente)
    freordenada = frecuencia;
    freordenada = sortrows(frecuencia, -1);
end