function cifradoafin = afin(clave, d, texto)
% Encripta una cadena de texto empleando la tecnica del cifrado afin
% utilizando los parametros de entrada como claves.
    
    % TRATAMIENTO DE ENTRADA
    % Las entradas deben ser numericas
    if ~isnumeric(clave) 
        error('Error: clave debe ser un dato numerico.');
    end    
    if ~isnumeric(d)
        error('Error: d debe ser un dato numerico.');
    end
    
    % Las entradas deben ser enteros
    if mod(clave,1) ~= 0
        error('Error: clave debe ser un numero entero.');
    end
    if mod(d,1) ~= 0
        error('Error: d debe ser un numero entero.');
    end
    
    % Clave debe tener inverso (mcd(clave,27) = 1)
    if gcd(clave,27) ~= 1
        error('Error: clave debe tener inverso mod 27');
    end
    
    % Texto debe ser una cadena
    if ~ischar(texto)
        error('Error: texto debe ser una cadena de caracteres.');
    end
   
    
    % CIFRADO AFIN DE MENSAJE
    secreto_numero = letranumero(texto);
    secreto_numero = secreto_numero * clave + d; 
    secreto_numero = mod(secreto_numero,27);
    
    % Conversion de secreto numerico a texto
    cifradoafin = '';
    abecedario = 'abcdefghijklmnñopqrstuvwxyz';
    % Soluciona conflicto caracter ñ convirtiendolo a ASCII
    abecedario(15) = char(241);
    
    % Construccion de cadena
    for i=1:size(secreto_numero, 2)
        cifradoafin = strcat(cifradoafin, abecedario(secreto_numero(i)+1));
    end
end