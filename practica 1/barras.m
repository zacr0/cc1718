function comparo = barras(v)
% Compara las frecuencias del criptograma v (en castellano), mostrando la
% comparacion mediante tabla y mediante diagrama de barras (histograma).

    % TRATAMIENTO DE ENTRADA
    % v debe ser una cadena
    if ~ischar(v)
        error('Error: v debe ser una cadena de caracteres.');
    end
    
    
    % ANALISIS DE FRECUENCIAS
    % Frecuencia relativa de caracteres del castellano
    freq_caste = [10.60 1.16 4.85 5.87 13.11 1.13 1.40 0.60 7.16 0.25 ... 
        0.11 4.42 3.11 7.14 0.10 8.23 2.71 0.74 6.95 8.47 5.40 4.34 0.82 ...
        0.12 0.15 0.79 0.26];
    freq_caste = freq_caste/100;
    
    % Frecuencia relativa de caracteres del criptograma
    [freq_cripto, freq_cripto_orden] = cripto_ana_orden(v);

    
    % CONSTRUCCION DE TABLA
    % Agrega fila con indices de las letras del alfabeto
    for i=1:numel(freq_caste)
        freq_caste(2,i) = i-1;
    end
    freq_caste = freq_caste';
    
    % Ordenamiento por frecuencia (descendente)
    freq_caste = sortrows(freq_caste, -1);
    
    % Union de valores en una tabla 27x4 con frecuencias e indices del 
    % castellano y criptograma
    comparo = table(freq_caste(:,1), freq_caste(:,2), freq_cripto_orden(:,1), ...
        freq_cripto_orden(:,2), 'VariableNames', {'Castellano', 'LetraCaste', 'Cifrado', 'LetraCifra'});
    
    
    % OBTENCION DE DIAGRAMAS DE BARRAS
    figure('Name', 'Frecuencias en castellano');
    bar(freq_caste(:,2), freq_caste(:,1), 'FaceColor', 'red');
    figure('Name', 'Frecuencias del criptograma');
    bar(freq_cripto_orden(:,2), freq_cripto_orden(:,1), 'FaceColor', 'blue');
   
end