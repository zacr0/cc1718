function criptoanalisis_afin(v,n)
% Realiza el criptoanalisis de un mensaje cifrado con el metodo afin,
% desconociendo la clave privada utilizada.

    % TRATAMIENTO DE ENTRADA
    % v debe ser una cadena
    if ~ischar(v)
        error('Error: v debe ser una cadena de caracteres.');
    end
    
    % n debe ser un numero
    if ~isnumeric(n)
        error('Error: n debe ser un dato numerico.');
    end
    % n debe ser un entero
    if mod(n,1) ~= 0
        error('Error: n debe ser un entero.');
    end
    
    
    % CRIPTOANALISIS
    % Analisis de frecuencias del mensaje
    frecuencias = barras(v);
    % Eliminacion de columnas innecesarias
    frecuencias = [frecuencias.LetraCaste, frecuencias.LetraCifra];
    
    % Tamaño del abecedario
    n = size(frecuencias(:,1),1);
    
    % Busqueda de claves interactuando con el usuario
    % Matriz de descifrado
    A = ones(2);

    % Obtencion de caracteres con mayor frecuencia siguientes
    A(:,1) = frecuencias(1:2, 1); % caracteres correspondientes en frecuencia

    % Calculo de inversa modular
    inv_A = inv_modulo(A, n);
    
    % Bucle de busqueda de claves
    continuar = 1;  % control de salida del bucle
    clave = -1; % clave de cifrado
    d = -1;     % desplazamiento de cifrado
    i = 1;      % indice de tabla de frecuencias castellano
    j = 2;      % indice de tabla de frecuencias criptograma
    it = 1;     % intentos de busqueda
    while continuar ~= 0 && i <= n
        while continuar ~= 0 && j <= n
            if i ~= j
                % Obtencion de caracteres con mayor frecuencia siguientes
                Y = [frecuencias(i, 2); frecuencias(j, 2)]; % caracteres del criptograma

                % Calculo de clave
                Y = inv_A * Y;
                clave = mod(Y(1,1), n);
                d = mod(Y(2,1), n);
                
                % Descifrado con clave calculada
                [mcd, alpha, beta] = gcd(clave, n);
                % Comprobar si tiene inversa
                if mcd == 1 
                    descifraafin = desafin(clave, d, v);
                    fprintf('Intento = %d (clave = %d, d = %d).\nMensaje: %s\n', it, clave, d, descifraafin);
                
                    % Consulta al usuario si continua la busqueda    
                    continuar = input('-- ¿Quieres probar otra clave? (1 = si, 0 = no): ');
                    if continuar > 1
                        continuar = 1;
                    end
                    
                    % Conteo de intentos
                    it = it+1;
                end
            end
            
            % Avance en tabla de frecuencias
            j = j+1;
        end
        % Siguientes elementos a probar de la tabla de frecuencias
        i = i+1;
        j = 1;
    end
    
    % Cierra figuras abiertas
    close all;
end