function descifraafin = desafin(clave, d, texto)
% Descifra un mensaje cifrado mediante la tecnica del cifrado afin,
% empleando la clave privada introducida como parametro.
    
    % TRATAMIENTO DE ENTRADA
    % Las entradas deben ser numericas
    if ~isnumeric(clave) 
        error('Error: clave debe ser un dato numerico.');
    end    
    if ~isnumeric(d)
        error('Error: d debe ser un dato numerico.');
    end
    
    % Las entradas deben ser enteros
    if mod(clave,1) ~= 0
        error('Error: clave debe ser un numero entero.');
    end
    if mod(d,1) ~= 0
        error('Error: d debe ser un numero entero.');
    end
    
    % Texto debe ser una cadena
    if ~ischar(texto)
        error('Error: texto debe ser una cadena de caracteres.');
    end
    
    
    % DESCIFRADO DE MENSAJE
    mensaje = letranumero(texto);
    descifraafin = '';
    abecedario = 'abcdefghijklmnñopqrstuvwxyz';
    % Soluciona conflicto caracter ñ convirtiendolo a ASCII
    abecedario(15) = char(241);
    
    % Obtencion del inverso de clave mediante la identidad de Bezout
    [mcd, alpha, beta] = gcd(clave, size(abecedario, 2));
    % Comprobar si tiene inversa
    if mcd ~= 1
        fprintf('%d NO es inversible en mod %d\n', clave, d);
    end
    clave = mod( alpha, size(abecedario, 2) );
    
    % Funcion de descifrado
    mensaje = (mensaje - d) * clave;
    mensaje = mod(mensaje, size(abecedario, 2));
    
    % Conversion de secreto a texto
    for i=1:size(mensaje,2)
        descifraafin = strcat( descifraafin, abecedario(mensaje(i)+1) );
    end
end