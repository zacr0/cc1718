function matrizclave=cripto_hill(claro,cripto,m)
% Aplica la tecnica de criptoanalisis conocida para el metodo de cifrado
% Hill, empleando el ataque por Gauss Jordan.
% De este modo, dado un mensaje claro y el cifrado asociado (o al menos 
% longitud_de_bloque^2 caracteres), se puede encontrar la clave utilizada y
% romper el cifrado.
    
    % TRATAMIENTO DE ENTRADA
    % claro debe ser una cadena de caracteres
    if ~ischar(claro)
        error('Error: claro debe ser una cadena de caracteres.');
    end
    % cripto debe ser una cadena de caracteres
    if ~ischar(cripto)
        error('Error: cripto debe ser una cadena de caracteres.');
    end
    
    % m debe ser un numero
    if ~isnumeric(m)
        error('Error: m debe ser un dato numerico.');
    end
    % m debe ser un entero
    if mod(m,1) ~= 0
        error('Error: m debe ser un entero.');
    end
    
    %m es el orden de la matriz
    matrizclave=[];
    claro=letranumero(claro);
    cripto=letranumero(cripto);
    nclaro=length(claro);
    ncripto=length(cripto);
    disp(ncripto);
    nrelleno = ceil(nclaro/m) * m;
    
    % Error: no se comprueba si se tiene un criptograma lo suficientemente
    % extenso como para realizar el criptoanalisis
    if ncripto >= m^2
        % Error: no se introducia relleno
        % Si el mensaje no es divisible en bloques, se introduce relleno
        if nclaro ~= nrelleno
            for i=nclaro+1:nrelleno
                % w no es un caracter comun en castellano
                claro(i) = letranumero('w');
            end
        end

        % VA A PONER EL TEXTO CLARO Y EL CIFRADO EN FILAS DE m columnas
        claro=reshape(claro,m,[])'; % error: intercambiadas filas y columnas
        crip=reshape(cripto,m,[])'; % error: intercambiadas filas y columnas

        % HACEMOS OPERACIONES ELEMENTALES HASTA QUE LA MATRIZ DEL TEXTO CLARO SEA I
        for j=1:m
            % Error: no se comprueba si pivote tiene inverso
            for k=j:nrelleno/m
                % Busqueda de pivote inversible en la matriz
                [G,AA,B]=gcd(27,claro(k,j));

                if G == 1
                    % Pivote es inversible, intercambiar filas si es necesario
                    if k ~= j
                        % Almacena filas temporalmente para el intercambio
                        temp = [claro(j,:); crip(j,:)];
                        % Intercambio de filas
                        claro(j,:) = claro(k,:);
                        claro(k,:) = temp(1,:);
                        crip(j,:) = crip(k,:);
                        crip(k,:) = temp(2,:);
                    end
                    break;
                else
                    if k == nrelleno/m
                        % No se ha encontrado un pivote inversible
                        error('No es posible aplicar el ataque por Gauss Jordan a la matriz indicada. Necesita un criptograma mas extenso.');
                    end
                end
            end

            %EL PIVOTE, LO HACEMOS 1 MULTIPLICANDO LA FILA POR SU INVERSO
            inv11=mod(B,27);
            claro(j,:)=mod(inv11*claro(j,:),27);
            crip(j,:)=mod(inv11*crip(j,:),27);

            %HACEMOS CEROS DEBAJO Y ENCIMA DEL PIVOTE
            % error: no se tenia en cuenta casos donde se ha rellenado
            for i=1:nrelleno/m
                if i~=j
                    crip(i,:)=mod(crip(i,:)-claro(i,j).*crip(j,:),27);
                    claro(i,:)=mod(claro(i,:)-claro(i,j).*claro(j,:),27);
                end
            end
        end
        matrizclave=crip(1:m,:)';
    else
        error('Error: cripto debe tener al menos m^2 caracteres.');
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % ESTA FUNCION SE LA DOY A ELLOS 
    % A=[11 13 0 7 1; 2 8 13 24 27; 7 19 26 25 2;9 5 7 21 14;1 0 0 0 6]
    % texto='lunes diez y media noche'
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
