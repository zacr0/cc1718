function [texto, cifrado] = cifro_permutacion(p, texto)
% Aplica el cifrado de permutacion (caso concreto del cifrado Hill) a un
% texto, devolviendo tanto el texto cifrado como el resultante de aplicar
% el cifrado, si es posible.

    % TRATAMIENTO DE ENTRADA
    % texto debe ser una cadena de caracteres
    if ~ischar(texto)
        error('Error: texto debe ser una cadena de caracteres.');
    end
    
    
    % CIFRADO DE PERMUTACION
    if permutacion_v(p)
        % Matriz de permutacion
        mat_p = matper(p);
        
        % Seleccion de cifrado/descifrado
        opcion = -1;
        while opcion ~= 1 && opcion ~= 2
            opcion = input('- Operacion a realizar. 1 - Cifrar. 2 - Descifrar: ');
        end
        
        switch opcion
            case 1
                % Cifrado
                cifrado = cifro_hill(mat_p, 27, texto); % 27: abecedario castellano
            case 2
                % Descifrado
                cifrado = texto;
                inv_mat_p = inv_modulo(mat_p, 27);
                texto = cifro_hill(mat_p, 27, texto);
            otherwise
                error('Error: opción invalida.');
        end
    end
end