function permuta = permutacion_v(p)
% Comprueba que un vector p representa una permutacion.
% En caso afirmativo, permuta = 1.

    % Inicializacion de salida
    permuta = 0;

    % TRATAMIENTO DE ENTRADA
    % p debe ser un vector
    if size(p, 1) > 1
        error('Error: p debe ser un vector.');
    end
    % A debe tener coeficientes numericos
    if ~isnumeric(p)
        error('Error: p debe ser un vector de numeros.');
    end
    % Los coeficientes numericos deben ser enteros
    if any(mod(p(:),1) ~= 0)
        error('Error: Los valores de p deben ser enteros.');
    end
    
    
    % COMPROBACION DE PERMUTACION
    % Se genera un vector con longitud tamaño de p que representa la
    % permutacion elemental, es decir, la permutacion en la que todos los
    % elementos estan ordenados.
    % Despues se ordena p y se compara la igualdad, en caso positivo, se
    % trata de una permutacion.
    vector_test = 1:length(p);
    if all(vector_test == sort(p))
        permuta = 1;
    else
        error('Error: p no es una permutacion.');
    end
    
end