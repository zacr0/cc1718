function cifrado = cifro_hill(A,m,texto)
% Cifra un texto mediante el metodo del cifrado Hill, empleando para ello
% una matrix clave A, sobre un alfabeto de longitud m.

    % TRATAMIENTO DE ENTRADA
    % A debe tener coeficientes numericos
    if ~isnumeric(A)
        error('Error: A debe ser una matriz de solo numeros.');
    end
    % Los coeficientes numericos deben ser enteros
    if any(mod(A(:),1) ~= 0)
        error('Error: Los coeficientes de A deben ser enteros.');
    end
    % A debe ser cuadrada
    if size(A) ~= size(A') 
        error('Error: A debe ser una matriz cuadrada.');
    end
    % Los elementos de A deben pertenecer al alfabeto
    if any(A(:) >= m)
        error('Error: A solo puede contener elementos del alfabeto elegido.');
    end
    % A debe ser inversible modulo m (coprimos)
    if gcd(round( det(A) ), m) ~= 1
        error('Error: A no es inversible en modulo %d.', m);
    end
    
    % m debe ser un numero
    if ~isnumeric(m)
        error('Error: m debe ser un dato numerico.');
    end
    % m debe ser un entero
    if mod(m,1) ~= 0
        error('Error: n debe ser un entero.');
    end
    
    % texto debe ser una cadena de caracteres
    if ~ischar(texto)
        error('Error: texto debe ser una cadena de caracteres.');
    end
    
    
    % CIFRADO HILL
    mensaje = letranumero(texto);
    % Longitud del mensaje
    long_msj = length(mensaje);
    % Longitud de bloques en los que se descompone el mensaje
    long_bloque = size(A, 1);
    % Numero de bloques (redondeado hacia arriba)
    n_bloques = ceil(long_msj / long_bloque);
    % Longitud del mensaje con relleno
    long_relleno = n_bloques * long_bloque;
    
    % Si el mensaje no es divisible en bloques, se introduce relleno
    if long_msj ~= long_relleno
        for i=long_msj+1:long_relleno
            % w no es un caracter comun en castellano
            mensaje(i) = letranumero('w'); 
        end
        % Actualiza longitud del mensaje
        long_msj = long_relleno;
    end
    
    % Division del mensaje en bloques
    % Array de bloques del mensaje original de long_bloque x n_bloques 
    X = reshape(mensaje, long_bloque, n_bloques);
    % Array de bloques del mensaje cifrado de long_bloque x n_bloques 
    Y = zeros(long_bloque, n_bloques);
    
    % Cifrado por columnas (Y=A*X mod m)
    for i=1:n_bloques
        Y(:,i) = A*X(:,i);
    end
    Y = mod(Y, m);
    
    % Paso de mensaje cifrado numerico a letras
    % Abecedario utilizado (castellano)
    abecedario = 'abcdefghijklmnñopqrstuvwxyz';
    % Soluciona conflicto caracter ñ convirtiendolo a ASCII
    abecedario(15) = char(241);
    
    % Paso de matriz a vector
    Y = reshape(Y, 1, long_msj);
    
    % Conversion de secreto a texto
    cifrado = '';
    for i=1:long_msj
        cifrado = strcat( cifrado, abecedario(Y(i)+1) );
    end
end
