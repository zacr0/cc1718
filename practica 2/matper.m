function mat_p=matper(p)
% Construye la matriz asociada a la permutacion p, si lo es.

    if permutacion_v(p)
        % Inicializacion de salida
        mat_p = zeros( length(p) );
        
        for i=1:length(p)
            mat_p(i, p(i)) = 1;
        end
    end
end