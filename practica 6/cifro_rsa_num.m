function cifrado = cifro_rsa_num(e, n, bloque_numero)
% Aplica el metodo de cifrado RSA a un bloque numerico, para lo cual emplea las
% claves publicas (n y e) del emisor.
% Se obtiene como salida el mensaje cifrado como un vector de bloques numéricos.
    
    % TRATAMIENTO DE ENTRADA
    % bloque_numero debe ser un vector con 1 columna
    if size(bloque_numero, 2) > 1
        error('Error: bloque_numero debe tener solo una columna.');
    end
    
    % CIFRADO RSA
    cifrado = zeros(size(bloque_numero));
    % Cifrado de cada bloque
    for i=1:length(bloque_numero)
        cifrado(i) = potencia(bloque_numero(i), e, n);
    end
    
end