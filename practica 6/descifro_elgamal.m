function desci = descifro_elgamal(a, y1, y2, q)
% Descifra un mensaje cifrado mediante el metodo ElGamal, partiendo de su 
% representacion numerica cifrado_numeros.
% Para ello se emplean a y q, claves privadas, e y1, obtenido durante el
% cifrado.
% Se obtiene como salida el mensaje original en texto plano.

    % Calculo del inverso de y1^a
    yla = double(potencia(y1, a, q)); % ajuste de tipos para potencia
    invyla = inv_modulo(yla, q);
    
    % Calculo del valor con el que se descifra el mensaje
    descifro = mod(y2 * invyla, q);
    
    % Recuperacion de mensaje original
    desci = num_descifra(q, descifro');
end