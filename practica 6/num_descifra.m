function desci = num_descifra(n, bloque_numero)
% Convierte un vector de bloques en formato numerico bloque_numero a su
% representacion equivalente con letras.
% Se recibe como entrada el valor de n, de manera que el tamaño de los 
% bloques nunca sera superior al número de digitos de n.
    
    % TRATAMIENTO DE ENTRADA
    % n debe ser numerico
    if ~isnumeric(n)
        error('Error: n debe ser un número.');
    end
    % n debe ser entero
    if mod(n, 1) ~= 0
        error('Error: n debe ser un entero.');
    end
    % n debe ser positivo
    if n < 0
        error('Error: n debe ser positivo.');
    end
    
    % bloque_numero debe ser un vector con 1 columna
    bloque_numero = bloque_numero';
    if size(bloque_numero, 2) > 1
        error('Error: bloque_numero debe tener solo una columna.');
    end
    
    
    % CONVERSION DE NUMEROS A LETRAS
    % Numero de cifras de n
    dign = num_cifras(n);
    % Tamaño de los bloques en los que se divide el mensaje
    tama = dign - 1;
    
    % Los elementos de bloque_numero deben tener una longitud maxima de
    % dign digitos
    for i=1:length(bloque_numero)
        if num_cifras(bloque_numero(i)) > dign
            error('Error: los bloques no deben superar la longitud máxima %d.', dign);
        end
    end
    % Preprocesado de bloques
    bloque_procesado = relleno_bloque(tama, bloque_numero);
    
    % Eliminar '0' del ultimo bloque (agregado como relleno cuando el 
    % bloque tenia tamaño impar)
    if mod(length(bloque_procesado), 2) ~= 0
        bloque_procesado = bloque_procesado(1:length(bloque_procesado)-1);
    end
    
    % Agrupacion en bloques de 2 (2 cifras = 1 letra)
    bloque_procesado = reshape(bloque_procesado, 2, [])';
    
    % Conversion de cadena a numero
    bloque_procesado = str2num(bloque_procesado);

    % Eliminacion de los '30' usados como relleno
    while bloque_procesado(size(bloque_procesado, 1)) == 30
        % Se elimina el ultimo elemento
        bloque_procesado = bloque_procesado(1:size(bloque_procesado, 1) - 1);
    end
        
    % Obtencion de mensaje en texto plano
    desci = num2alfabeto(bloque_procesado);
end