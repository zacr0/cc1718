function texto_numerico = letranumero(texto_letra)
% Convierte una cadena de texto a su equivalente numerico utilizando
% el abecedario castellano.
    
    % TRATAMIENTO DE ENTRADA
    % texto_letra debe ser una cadena
    if ~ischar(texto_letra)
        error('Error: debe introducir una cadena de caracteres.');
    end

    % Inicializacion de salida
    texto_numerico = [];
    
    % Abecedario utilizado (castellano)
    abecedario = 'abcdefghijklmnñopqrstuvwxyz';
    % Soluciona conflicto caracter ñ convirtiendolo a ASCII
    abecedario(15) = char(241);
    
    % Convierte la entrada a minusculas
    texto_letra = lower(texto_letra);
   
    % Busqueda caracter por caracter en el abecedario
    for i=1:size(texto_letra, 2)
        for j=1:size(abecedario, 2)
            if (texto_letra(i) == abecedario(j))
                % Almacenamiento de indice en salida
                texto_numerico = [texto_numerico, j-1];
                break
            end
        end
    end
    
end