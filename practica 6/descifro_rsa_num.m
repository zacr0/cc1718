function descifrado = descifro_rsa_num(d, n, cifrado_numeros)
% Descifra un bloque numerico cifrado_numeros, cifrado mediante RSA, empleando
% las claves privadas del receptor, d y n.
% Devuelve un bloque numerico con cada elemento descifrado.
    
    % Elevar cada elemento cifrado con la clave privada para deshacer el
    % cifrado RSA
    descifrado = zeros(size(cifrado_numeros));
    for i=1:length(descifrado)
        descifrado(i) = potencia(cifrado_numeros(i), d, n);
    end
end