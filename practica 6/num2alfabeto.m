function texto = num2alfabeto(texto_numerico)
% Convierte un texto en su representacion numerica a su equivalente en el
% alfabeto castellano de 27 caracteres.
    
    % TRATAMIENTO DE ENTRADA
    if (any(texto_numerico(:) > 27))
        error('Error: texto_numerico contiene elementos superiores a 27.');
    end
    
    % CONVERSION A CARACTERES DEL ALFABETO
    alfabeto = 'abcdefghijklmnñopqrstuvwxyz';
    alfabeto(15) = char(241);
    texto = alfabeto(texto_numerico(:)+1);
end