function blo = prepa_num_cifrar(tama, bloque)
% Convierte un bloque (cadena formada por numeros) en bloques de tamaño
% fijo tama. Estos se convierten en bloques de numeros que seran
% introducidos en la salida, blo.
    
    % TRATAMIENTO DE ENTRADA
    % tama debe ser numerico
    if ~isnumeric(tama)
        error('Error: tama deben ser un número.');
    end
    % tama deben ser un entero
    if mod(tama, 1) ~= 0
        error('Error: tama debe ser un entero.');
    end
    % tama deben ser positivo
    if tama < 0
        error('Error: tama deben ser positivo.');
    end
    % bloque debe ser una cadena
    if ~ischar(bloque)
        error('Error: bloque debe ser una cadena de caracteres.');
    end
    
    
    % CONVERSION EN BLOQUES
    % Introduccion de relleno (si es necesario)
    if mod(length(bloque), tama) ~= 0
        % Es necesario introducir relleno
        aux = tama - mod(length(bloque), tama);

        % Si es par, se rellena con '30'
        if mod(aux,2) == 0
            while mod(length(bloque), tama) ~= 0
                bloque = strcat(bloque, '30');
            end
        else 
            % Si es impar, se rellena con '30' 
            while aux ~= 1
                bloque = strcat(bloque, '30');
                aux = tama - mod(length(bloque), tama);
            end
            % Y ademas se rellena con '0'
            bloque = strcat(bloque, '0');
        end
    end
    
    % Descomposicion en bloques de n_bloques x long_bloque
    bloque = reshape(bloque, tama, [])';
    blo = str2num(bloque);
end