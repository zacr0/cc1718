function genero_clave(p, q, e)
% Genera las claves publica y privadas necesarias para cifrar con RSA.
% Para ello, p y q deben ser dos numeros enteros positivos primos, de los
% cuales se calcula su funcion phi de Euler.
% Ademas, se selecciona otro numero e, que formara parte de la clave
% privada y que debe ser primo relativo con la phi de Euler obtenida.
% Se mostrara como salida tanto la clave publica como la privada.
    % TRATAMIENTO DE ENTRADA
     % Las entradas debe ser numericas
    if ~isnumeric([p, q, e])
        error('Error: los valores de entrada deben ser números.');
    end
    % Las entradas deben ser enteros
    if any(mod([p, q, e], 1) ~= 0)
        error('Error: los valores de entrada deben ser enteros.');
    end
    % Las entradas deben ser positivas
    if any([p, q, e] < 0)
        error('Error: los valores de entrada deben ser positivos.');
    end
    % p y q deben ser primos
    if any(isprime([p, q]) ~= 1)
        error('Error: p y q deben ser numeros primos.');
    end
    
    
    % CALCULO DE CLAVES RSA
    n = p*q;
    % phi de Euler
    fiden = (p-1)*(q-1);
    
    % Busqueda de d (inverso de e mod fiden)
    [G, d, ~] = gcd(e, fiden);
    d = mod(d, fiden);
    % e debe ser coprimo con la phi de Euler
    if G ~= 1
        error('Error: e debe ser coprimo de la phi de Euler de p y q.');
    end
    
    % Mostrar claves obtenidas
    fprintf('La clave privada es (n, d) = (%d, %d).\n', n, d);
    fprintf('La clave publica es (n, e) = (%d, %d).\n', n, e);
end