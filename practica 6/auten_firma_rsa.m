function auten_firma_rsa(na, ea, da, nb, eb, db, mensaje, firma)
% Representa un intercambio de mensajes cifrados bajo el metodo RSA, 
% empleando la autentificacion de firma del receptor para validar la
% autenticidad del mensaje enviado.
% Como entrada se recibe:
%   - La clave publica y privada de A: na, ea, da.
%   - La clave publica y privada de B: nb, eb, db.
%   - El mensaje a enviar cifrado mediante RSA.
%   - La firma del emisor del mensaje.

    % Paso 1. Generacion de claves privada y publica de los interlocutores
    % A y B.
    fprintf('- A y B han generado previamente sus claves RSA.\n');
    fprintf('- A: clave pública(%d, %d), clave privada(%d, %d).\n', na, ea, na, da);
    fprintf('- B: clave pública(%d, %d), clave privada(%d, %d).\n', nb, eb, nb, db);
    
    
    % Paso 2. Envio desde A del mensaje firmado, junto con la firma, ambos
    % cifrados.
    % Agregar firma al mensaje original
    mensaje = strcat(mensaje, firma);
    % Cifrado del mensaje firmado con la clave publica de B
    cif_mens_f = cifro_rsa(eb, nb, mensaje);
    % Cifrado de firma de A con su clave privada
    cif_f_da = cifro_rsa(da, na, firma);
    
    % Preparacion de los bloques para que se ajusten al tamaño de bloque
    % requerido para cifrarlo con la clave publica de b (nb)
    bloque_procesado = relleno_bloque(num_cifras(na), cif_f_da);
    blo = prepa_num_cifrar(num_cifras(nb)-1, bloque_procesado);
    
    % Cifrado de firma de A con la clave publica de B
    cif_f_da_eb = cifro_rsa_num(eb, nb, blo);
    
    % Envio de informacion al receptor
    fprintf('- A envia a B el mensaje firmado y cifrado con RSA:\n');
    disp(cif_mens_f');
    fprintf('- A envia a B la firma cifrada con su clave publica y la clave publica de B:\n');
    disp(cif_f_da_eb');
    
    
    % Paso 3. Descifrado del mensaje y firma por parte de B.
    % Recuperacion del mensaje original
    % Descifrado del mensaje firmado con su clave privada
    mensaje_descifrado = descifro_rsa(db, nb, cif_mens_f');
    
    % Descifrado de firma con su clave privada (obtiene mensaje aun incomprensible)
    descifr_f_da_eb = descifro_rsa_num(db, nb, cif_f_da_eb);
    
    % Preparacion de bloque para que se adapte al tamaño de la clave
    % publica de A
    bloque_procesado = prepa_bloque_descif(nb, num_cifras(na), descifr_f_da_eb);
    
    % Descifrado con la clave publica de A
    firma_descifrada = descifro_rsa(ea, na, bloque_procesado');
    
    % Recepcion de informacion descifrada en B
    fprintf('- B recibe el mensaje firmado enviado por A: %s.\n', mensaje_descifrado);
    fprintf('- B recibe la firma de A: %s.\n', firma_descifrada);
    
    
    % Paso 4. Validacion de firma.
    % Extraccion de firma del mensaje
    firma_mensaje = mensaje_descifrado(length(mensaje_descifrado)+1-length(firma_descifrada):length(mensaje_descifrado));
    if strcmp(firma_descifrada, firma_mensaje)
        fprintf('- Las firmas coinciden, el mensaje es VALIDO.\n');
    else
        fprintf('- Las firmas no coinciden, el mensaje ha sido comprometido.\n');
    end
end