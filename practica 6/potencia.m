function pote = potencia(c, d, n)
% Calcula la potencia modular de un número de (posiblemente) gran magnitud
% empleando el algoritmo de potenciación rápida.
% c es la base, d el exponente y n el modulo en el que se calcula la
% potencia modular.

    % TRATAMIENTO DE ENTRADA
    % Las entradas debe ser numericas
    if ~isnumeric([c, d, n])
        error('Error: los valores de entrada deben ser números.');
    end
    % Las entradas deben ser enteros
    if any(mod([c, d, n], 1) ~= 0)
        error('Error: los valores de entrada deben ser enteros.');
    end
    % Las entradas deben ser positivas
    if any([c, d, n] < 0)
        error('Error: los valores de entrada deben ser positivos.');
    end
    
    
    % ALGORITMO DE POTENCIACIÓN RÁPIDA
    % Fijar el tamaño de variable
    c = uint64(c);
    %d = uint64(d);
    n = uint64(n);
    
    % Paso de exponente a binario
    d_bin = dec2bin(d);
    % Inversion del vector de izquierda a derecha
    d_bin = fliplr(d_bin);
    
    % Obtencion de las potencias de c
    potencias_c = uint64(zeros(1, length(d_bin)));
    potencias_c(1) = mod(c, n);
    for i=2:length(potencias_c)
        potencias_c(i) = mod(potencias_c(i-1)^2, n);
    end
    
    % Producto modular para obtener la potencia final
    pote = 1;
    for i=1:length(d_bin)
        if d_bin(i) == '1'
            pote = uint64(mod(pote * potencias_c(i), n));
        end
    end
end