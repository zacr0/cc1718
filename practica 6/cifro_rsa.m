function cifrado = cifro_rsa(e, n, texto)
% Aplica el metodo de cifrado RSA a un texto, para lo cual emplea las
% claves publicas (n y e) del emisor.
% Se obtiene como salida el mensaje cifrado como un vector de bloques numéricos.

    % Conversion de texto a su equivalente numérico (2 cifras)
    texto_num = letra2numeros(texto);
    
    % Numero de cifras de n
    dign = num_cifras(n);
    % Tamaño de los bloques en los que se divide el mensaje
    tama = dign - 1;
    
    % División de los bloques en formato numérico
    blo = prepa_num_cifrar(tama, texto_num);
    
    % Cifrado de cada bloque (elevando a e modulo n)
    for i=1:length(blo)
        cifrado(i,:) = potencia(blo(i,:), e, n);
    end
end