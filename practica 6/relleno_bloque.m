function bloque_procesado = relleno_bloque(n, bloque)
% Genera una cadena a partir de un conjunto de bloques numericos tras
% introducirle el relleno necesario para alcanzar el tamaño n.
% Devuelve la cadena formada.

    % TRATAMIENTO DE ENTRADA
    % n debe ser numerico
    if ~isnumeric(n)
        error('Error: n debe ser un número.');
    end
    % n debe ser entero
    if mod(n, 1) ~= 0
        error('Error: n debe ser un entero.');
    end
    % n debe ser positivo
    if n < 0
        error('Error: n debe ser positivo.');
    end
    
    % bloque debe ser un vector con 1 columna
    if size(bloque, 2) > 1
        error('Error: bloque debe tener solo una columna.');
    end
    
    % PROCESAMIENTO DE BLOQUE
    bloque_procesado = '';
    for i=1:length(bloque)
        bloque_aux = num2str(bloque(i));
        
        while length(bloque_aux) < n
            % Relleno con '0' hasta completar el tamaño del bloque
            bloque_aux = strcat('0', bloque_aux);
        end
        % Agregar sub-bloque procesado a la cadena completa
        bloque_procesado = strcat(bloque_procesado, bloque_aux);
    end
end