function doble = letra2numeros(texto)
% Dado una cadena de texto introducida como entrada, se le asocia a cada
% una de las letras su correspondiente valor en Z_27 en formato de dos
% digitos.

    % Conversion del texto caracter a caracter
    doble = sprintf('%02d', letranumero(texto));
    
end