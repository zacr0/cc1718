function blo = prepa_bloque_descif(n, tama_b, bloque_numero)
% Convierte un vector de bloques en formato numerico bloque_numero en otro
% vector de numeros blo, que representa los bloques adaptados a un tamaño
% de clave concreto.
% Se recibe como entrada el valor de n, tal que el tamaño de los 
% bloques nunca sera superior al número de digitos de n.
% Se recibe como entrada el tamaño de la clave de B, para adaptar el bloque
% al tamaño de bloque de la clave de B.
    
    % TRATAMIENTO DE ENTRADA
    % n y tama deben ser numericas
    if any(~isnumeric([n tama_b]))
        error('Error: n y tama deben ser numeros.');
    end
    % n y tama deben ser enteros
    if any(mod([n tama_b], 1) ~= 0)
        error('Error: n y tama deben ser un entero.');
    end
    % n y tama debe ser positivos
    if n < 0
        error('Error: n y tama debe ser positivos.');
    end
    
    % bloque_numero debe ser un vector con 1 columna
    if size(bloque_numero, 2) > 1
        error('Error: bloque_numero debe tener solo una columna.');
    end
    
    
    % CONVERSION DE BLOQUES
    % Numero de cifras de n
    dign = num_cifras(n);
    % Tamaño de los bloques en los que se divide el mensaje
    tama = dign - 1;
    
    % Preprocesado de bloques
    bloque_procesado = '';
    for i=1:size(bloque_numero, 1)
        % Los elementos de bloque_numero deben tener una longitud maxima de
        % dign digitos
        if num_cifras(bloque_numero(i,:)) > dign
            error('Error: los bloques no deben superar la longitud máxima %d.', dign);
        end
        
        % Agregar '0' en los bloques de menor longitud a la maxima
        bloque_aux = num2str(bloque_numero(i));
        while length(bloque_aux) < tama
            bloque_aux = strcat('0', bloque_aux);
        end
        
        % Agregar bloque procesado
        bloque_procesado = strcat(bloque_procesado, bloque_aux);
    end
    
    % Eliminar '0' del ultimo bloque (agregado como relleno cuando el 
    % bloque tenia tamaño impar)
    if mod(length(bloque_procesado), 2) ~= 0
        bloque_procesado = bloque_procesado(1:length(bloque_procesado)-1);
    end
    
    % Agrupacion en bloques de 2 (2 cifras = 1 letra)
    bloque_procesado = reshape(bloque_procesado, 2, [])';
    
    % Eliminacion de los '30' usados como relleno
    while strcmp(bloque_procesado(size(bloque_procesado, 1), :), '30')
        % Se elimina el ultimo elemento
        bloque_procesado = bloque_procesado(1:size(bloque_procesado, 1) - 1, :);
    end
        
    % Agrupacion de bloque procesado en una sola cadena
    bloque = '';
    for i=1:length(bloque_procesado)
        bloque = strcat(bloque, bloque_procesado(i, :));
    end
    
    % Convierte el bloque en bloques numericos
    blo = prepa_num_cifrar(tama_b, bloque);
end