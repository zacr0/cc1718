function [y1, y2] = cifro_elgamal(q, g, a, k, texto)
% Cifra un texto mediante el metodo ElGamal.
% Para ello el interlocutor elige un entero k < q, del cual se calculara gk
% y gak, ambos en modulo q.
% Se obtiene como salida y1 = gk.
% Se obtiene como salida y2 = mod(blo*gak,q), donde blo es el vector de
% numeros resultante tras preparar los numeros para cifrar con
% prepa_num_cifrar.

    % TRATAMIENTO DE ENTRADA
    % Las entradas debe ser numericas
    if ~isnumeric([q, g, a, k])
        error('Error: los valores de entrada deben ser números.');
    end
    % Las entradas deben ser enteros
    if any(mod([q, g, a, k], 1) ~= 0)
        error('Error: los valores de entrada deben ser enteros.');
    end
    % Las entradas deben ser positivas
    if any([q, g, a, k] < 0)
        error('Error: los valores de entrada deben ser positivos.');
    end
    
    % k debe ser menor que q
    if k >= q 
        error('Error: k debe ser menor que q.');
    end
    
    
    % CIFRADO DE MENSAJE MEDIANTE ELGAMAL
    gk = potencia(g, k, q);
    gak = potencia(g, a*k, q);
    
    % Preparacion de mensaje para cifrado
    tama = num_cifras(q)-1;
    doble = letra2numeros(texto);
    blo = uint64(prepa_num_cifrar(tama, doble)); % ajuste de tipos para producto
    
    % Cifrado
    y1 = gk;
    y2 = mod(blo * gak, q);
    
end