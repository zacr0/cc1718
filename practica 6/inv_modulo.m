function inver = inv_modulo(A,n)
% Calcula la inversa de una matriz A de coeficientes enteros en modulo n
    % Inicializacion de salida
    inver = 0;
    error = 0;
    
    % TRATAMIENTO DE ENTRADA
    % A debe tener coeficientes numericos
    if ~isnumeric(A)
        fprintf('Error: A debe ser una matriz de solo numeros.\n');
        error = 1;
    end
    % Los coeficientes numericos deben ser enteros
    if any(mod(A(:),1) ~= 0)
        fprintf('Error: Los coeficientes de A deben ser enteros.\n');
        error = 1;
    end
    % A debe ser cuadrada
    if size(A) ~= size(A') 
        fprintf('Error: A debe ser una matriz cuadrada.\n');
        error = 1;
    end
    
    % n debe ser un numero
    if ~isnumeric(n)
        fprintf('Error: n debe ser un dato numerico.\n');
        error = 1;
    end
    % n debe ser un entero
    if mod(n,1) ~= 0
        fprintf('Error: n debe ser un entero.\n');
        error = 1;
    end
    
    
    % CALCULO DE MATRIZ INVERSA MODULO N
    if error == 0
        det_A = round( det(A) ); % round -> gcd requiere enteros

        % Comprobacion de existencia de inversa en mod n
        if gcd(det_A, n) ~= 1
            fprintf('Error: A no es inversible en modulo %d.\n', n);
            error = 1;
        end

        if error == 0
            % Cálculo de inversa modular
            [mcd, alpha, beta] = gcd(det_A, n); % alpha = det_A^-1 mod n
            inver = inv(A) * det_A * alpha; % no puede utilizarse \
            inver = round( mod(inver, n) ); % round -> evitar salida decimal
        end
    end
end
