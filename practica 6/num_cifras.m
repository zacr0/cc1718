function dign = num_cifras(n)
% Calcula el número de digitos que contiene un número n.
    % n debe ser numerico
    if ~isnumeric(n)
        error('Error: n debe ser un número.');
    end
    % n debe ser entero
    if any(mod(n, 1) ~= 0)
        error('Error: n deben ser un entero.');
    end
    
    % Calculo del número de cifras de n (leyendolo como cadena)
    dign = length( int2str(abs(n)) );
end