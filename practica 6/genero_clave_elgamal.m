function [a, clave_publica] = genero_clave_elgamal(q, g)
% Genera las claves publica (clave_publica) y privada (a) para un interlocutor
% empleando el metodo ElGamal.
% Para ello, se emplean las claves privadas acordadas, q y p. Donde q es un
% numero primo (se trabaja en Zp); y g es un numero distinto de 0 en Zq.
% Se obtiene como salida a, un entero aleatorio tal 0 < a < q-1.
% Se obtiene como salida la clave publica, tal que clave_publica = mod(g^a, q)
    
    % TRATAMIENTO DE ENTRADA
    % Las entradas debe ser numericas
    if ~isnumeric([q, g])
        error('Error: los valores de entrada deben ser números.');
    end
    % Las entradas deben ser enteros
    if any(mod([q, g], 1) ~= 0)
        error('Error: los valores de entrada deben ser enteros.');
    end
    % Las entradas deben ser positivas
    if any([q, g] < 0)
        error('Error: los valores de entrada deben ser positivos.');
    end
    
    
    % GENERACION DE CLAVES
    a = randi(q-2); % -2 porque el intervalo es inclusivo
    clave_publica = potencia(g, a, q);
end