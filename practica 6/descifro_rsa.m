function descifrado = descifro_rsa(d, n, cifrado_numeros)
% Descifra un texto cifrado_numeros en su representacion numerica, cifrado 
% mediante RSA, empleando las claves privadas del receptor, d y n.
% Devuelve el texto original en texto plano.
    
    % Elevar cada elemento cifrado con la clave privada para deshacer el
    % cifrado RSA
    valores_sin_cifrar = zeros(size(cifrado_numeros));
    for i=1:length(cifrado_numeros)
        valores_sin_cifrar(i) = potencia(cifrado_numeros(i), d, n);
    end
    
    % Conversion de numero a letra (texto original)
    descifrado = num_descifra(n, valores_sin_cifrar);
end