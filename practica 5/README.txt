En las funciones intercambio_diffie_hellman y cripto_diffie_hellman he añadido un nuevo parametro, seed, para fijar la semilla pseudoaleatoria de los números aleatorios.
En la cabecera del código viene documentado este mismo dato, con alguna indicación adicional.

Por tanto, los prototipos de las funciones pasan a ser:
	intercambio_diffie_hellman(g, p, semilla)
	cripto_diffie_hellman(g, p, semilla)
