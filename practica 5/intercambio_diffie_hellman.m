function intercambio_diffie_hellman(g, p, semilla)
% Simulacion de los pasos seguidos por dos interlocutores para el
% intercambio de mensajes encriptados mediante el metodo de Diffie y
% Hellman.
% Dado un numero generador g del conjunto Zp, se calculan las claves con
% los que los interlocutores cifraran sus respetivos mensajes.
%
% NOTA: se ha añadido un parametro adicional, semilla, para fijar la semilla
% pseudoaleatoria del programa. Este parametro esta destinado unicamente
% para las pruebas y en un entorno real no se utilizaria.
%

    % PASO 1. Acordar un numero g y un primo p
    % g debe ser generador del conjunto Zp
    if genera(g, p)
        
        % Fijar semilla generadora pseudoaleatoria
        rng(semilla);
        
        % PASO 2. Generacion de aleatorios de A y B
        aa = randi(p-1);
        fprintf('- A genera un número aleatorio. aa = %d\n', aa);
        pote_a = potencia(g, aa, p);
        fprintf('- A envía a B: %d\n', pote_a);
        
        bb = randi(p-1);
        fprintf('- B genera un número aleatorio. bb = %d\n', bb);
        pote_b = potencia(g, bb, p);
        fprintf('- B envía a A: %d\n', pote_b);
        
        % PASO 3. Calculo de clave de cifrado de mensajes
        clave_a = potencia(pote_b, aa, p);
        fprintf('- A obtiene la clave: %d\n', clave_a);
        
        clave_b = potencia(pote_a, bb, p);
        fprintf('- B obtiene la clave: %d\n', clave_b);
    else
        % No puede aplicarse el metodo
        error('- %d no es un elemento generador de %d.', g, p);
    end
    
end