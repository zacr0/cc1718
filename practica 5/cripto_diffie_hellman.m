function cripto_diffie_hellman(g, p, semilla)
% Demuestra la vulnerabilidad presente en el cifrado de Diffie y Hellman
% por la cual un intruso que intercepte los valores durante el intercambio
% de claves públicas puede llegar a calcular las claves privadas de cada interlocutor.
%
% NOTA: se ha añadido un parametro adicional, semilla, para fijar la semilla
% pseudoaleatoria del programa. Este parametro esta destinado unicamente
% para las pruebas y en un entorno real no se utilizaria.
%
    % PASO 1. Acordar un numero g y un primo p
    % g debe ser generador del conjunto Zp
    if genera(g, p)
        
        % Fijar semilla generadora pseudoaleatoria
        rng(semilla);
        
        % PASO 2. Generacion de aleatorios de A y B
        aa = randi(p-1);
        fprintf('- A genera un número aleatorio. aa = %d\n', aa);
        pote_a = potencia(g, aa, p);
        fprintf('- A envía a B: %d\n', pote_a);
        
        % C intercepta el valor y genera uno propio
        cc = randi(p-1);
        fprintf('- C genera un número aleatorio. cc = %d\n', cc);
        pote_c = potencia(g, cc, p);
        fprintf('- C (impersonando a A) envía a B: %d\n', pote_c);
        fprintf('- B piensa que le llega de A.\n');
        
        bb = randi(p-1);
        fprintf('- B genera un número aleatorio. bb = %d\n', bb);
        pote_b = potencia(g, bb, p);
        fprintf('- B envía a A: %d\n', pote_b);
        
        % C intercepta el valor y genera uno propio
        fprintf('- C (impersonando a B) envía a A: %d\n', pote_c);
        fprintf('- A piensa que le llega de B.\n');
        
        % PASO 3. Calculo de clave de cifrado de mensajes (con las claves
        % intrusas)
        clave_a = potencia(pote_c, aa, p);
        fprintf('- A obtiene la clave a partir del valor del intruso: %d\n', clave_a);
        clave_b = potencia(pote_c, bb, p);
        fprintf('- B obtiene la clave a partir del valor del intruso: %d\n', clave_b);
        
        % RESULTADO. C conoce las claves privadas de A y B
        clave_ca = potencia(pote_a, cc, p);
        fprintf('- C sabe que la clave de A es %d.\n', clave_ca);
        clave_cb = potencia(pote_b, cc, p);
        fprintf('- C sabe que la clave de B es %d.\n', clave_cb);
        
    else
        % No puede aplicarse el metodo
        error('- %d no es un elemento generador de %d.', g, p);
    end
end