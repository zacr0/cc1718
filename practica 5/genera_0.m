function generador = genera_0(g, p)
% Comprueba si g, un entero positivo, es generador del cuerpo generado por
% el primo p.
    % Inicializacion
    generador = g;
    
    % TRATAMIENTO DE ENTRADA
    % Las entradas debe ser numericas
    if ~isnumeric([g, p])
        generador = 0;
        fprint('Error: los valores de entrada deben ser números.');
    end
    % Las entradas deben ser enteros
    if any(mod([g, p], 1) ~= 0)
        generador = 0;
        fprintf('Error: los valores de entrada deben ser enteros.\n');
    end
    % Las entradas deben ser positivas
    if any([g, p] < 0)
        generador = 0;
        fprintf('Error: los valores de entrada deben ser positivos.\n');
    end
    % g debe estar en el conjunto
    if g <= 1 && g >= p
        generador = 0;
        fprintf('Error: g debe tener un valor en el intervalo [1, p-1].\n');
    end
    % p debe ser primo
    if ~isprime(p)
        generador = 0;
        fprintf('Error: p debe ser un número primo.\n');
    end
    
    % COMPROBACION DE ELEMENTO GENERADOR
    if generador
        % Inicio tiempo de computo
        tic

        % Calculo de potencias modulares de g
        potencias = [];
        for i=1:p-1            
            nuevo_elemento = potencia(g, i, p);
            
            if find(potencias == nuevo_elemento)
                % Elemento repetido, no es generador
                generador = 0;
                break;
            else
                potencias = [potencias, nuevo_elemento];
            end
        end
        
        % Fin tiempo de computo
        toc
    end
end