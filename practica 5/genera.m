function generador = genera(g, p)
% Determina si un entero positivo, g, es generador del cuerpo generado por
% el primo p.
% Para ello, se emplea un metodo distinto a la definicion clasica mediante
% el cual se optimiza dicho computo.
    
    % Inicio tiempo de computo
    tic
    
    % Inicializacion
    generador = g;
    
    
    % TRATAMIENTO DE ENTRADA
    % Las entradas debe ser numericas
    if ~isnumeric([g, p])
        generador = 0;
        fprint('Error: los valores de entrada deben ser números.');
    end
    % Las entradas deben ser enteros
    if any(mod([g, p], 1) ~= 0)
        generador = 0;
        fprintf('Error: los valores de entrada deben ser enteros.\n');
    end
    % Las entradas deben ser positivas
    if any([g, p] < 0)
        generador = 0;
        fprintf('Error: los valores de entrada deben ser positivos.\n');
    end
    % g debe estar en el conjunto
    if g <= 1 && g >= p
        generador = 0;
        fprintf('Error: g debe tener un valor en el intervalo [1, p-1].\n');
    end
    % p debe ser primo
    if ~isprime(p)
        generador = 0;
        fprintf('Error: p debe ser un número primo.\n');
    end
    
    % DETERMINAR SI EL ELEMENTO ES GENERADOR
    if generador
        % Factores primos de p-1
        fact_prim = primes(p-1);
        
        % Comprobacion de condicion de cada elemento
        for i=1:length(fact_prim)
            exp = uint64((p-1) / fact_prim(i));
            aux = potencia(g, exp, p);
            if aux == 1
                % No cumple la condicion, no es generador
                generador = 0;
                break;
            end
        end
    end
    
    % Fin tiempo de computo
    toc
end