function fhash = md5()
% Compute the MD5 digest of the message, as a hexadecimal digest.
% Follow the MD5 algorithm from RFC 1321 [1] and Wikipedia [2].
% [1] http://tools.ietf.org/html/rfc1321
% [2] http://en.wikipedia.org/wiki/MD5
% m is the modulus for 32-bit unsigned arithmetic.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %% PASO 1 INTRODUCIMOS VARIABLES Y CONSTANTES NECESARIAS
    clc
    %PASO1.1 ESCRIBIMOS EL MENSAJE
    mensaje = input('Introduzca el mensaje al que aplicar MD5: ', 's');
    
    %PASO 1.2.- VAMOS A TRABAJAR MODULO M=2^32
    m = 2^32;
    
    % PASO 1.3.- CREAMOS UNA MATRIZ S PARA HACER LA ROTACIÓN,
    % LOS NÚMEROS NEGATIVOS POR SER UNA ROTACIÓN A IZQUIERDA
    s = [-7, -12, -17, -22; ...
         -5, -9,  -14, -20; ...
         -4, -11, -16, -23; ...
         -6, -10, -15, -21];
    
    % PASO 1.4.- t ES LA TABLA QUE USAREMOS MÁS ADELANTE, para construir la función
    % Hash del mensaje
    t = fix( abs(sin(1:64)) .* m );
    
    % PASO 1.5.- INICIALIZAMOS LA HASH
    % MD5 utiliza las cuatro palabras siguientes:
    % A=01 23 45 67 pero en Little endian: 67 45 23 01
    % B=89 ab cd ef --> ef cd ab 89
    % C=fe dc ba 98 --> 98 ba cd fe
    % D=76 54 32 10 --> 10 32 54 76
    fhash = [hex2dec('67452301'), ...
             hex2dec('efcdab89'), ...
             hex2dec('98badcfe'), ...
             hex2dec('10325476')];
    
    %% PASO 2.- PREPARAMOS EL MENSAJE PARA APLICARLE LA HASH
    mensaje = abs(mensaje);
    bytelen = numel(mensaje); %número de elementos del vector
    
    % PASO 2.1.-
    % AÑADIMOS AL MENSAJE UN 1 Y LOS 0'S NECESARIOS PARA QUE EL NÚNERO DE BITS
    % SEA CONGRUENTE CON 448 MÓDULO 512
    % COMO TENEMOS BYTES, AÑADIMOS 128 (10000000) Y LOS CEROS NECESARIOS PARA QUE
    % EL NÚMERO DE BYTES SEA CONGRUENTE CON 56 MÓDULO 64
    mensaje = [mensaje, 128];
    while mod(numel(mensaje), 64) ~= 56
        % Agregar bloques de 0s hasta alcanzar congruencia 56 mod 64
        mensaje = [mensaje, 0];
    end
    
    % PASO 2.2.-
    % CÓMO CADA PALABRA ESTÁ FORMADA POR 4 BYTES, HACEMOS UNA MATRIZ DE 4 FILAS
    % CON LOS BYTES DEL MENSAJE, ASÍ CADA COLUMNA SERÁ UNA PALABRA
    mensaje = reshape(mensaje, 4, []);
    
    % PASO 2.3.-
    % CONVERTIMOS CADA COLUMNA A ENTEROS DE 32 BITS, little endian.
    % Se:
    %   Multiplica fila 1 por 1
    %   Multiplica fila 2 por 2^8
    %   Multiplica fila 3 por 2^16
    %   Multiplica fila 4 por 2^24
    % Y se suma cada columna.
    mensaje = sum( mensaje .* repmat(2.^[0 8 16 24]', [1, size(mensaje,2)]) );
    
    % PASO 2.4.-
    % AÑADIMOS LA LONG DEL MENSAJE ORIGINAL COMO UN ENTERO
    % DE 64 BITS __>8 bytes__>dos palabras : little endian.
    bitlen = bytelen * 8;
    mensaje = [mensaje, mod(bitlen, m), mod(floor(bitlen / m), m)];
    
    %% PASO 3. REALIZAMOS LA FUNCIÓN HASH
    % CADA BLOQUE DE 512 bit
    % TENEMOS ENTEROS DE 32 BITS (PALABRA)CADA BLOQUE TIENE 16 ELEMENTOS
    % (PALABRAS)
    % message(k + (0:15))
    for k = 1:16:numel(mensaje)
        % Guardar una copia de las palabras antes de operar sobre el bloque
        % para sumarlas posteriormente
        a = fhash(1); 
        b = fhash(2); 
        c = fhash(3); 
        d = fhash(4);
        
        % Realizar las 64 operaciones correspondientes a los 4 bloques
        for i=1:64
            % Convertimos b, c, d a vector fila of bits (0s and 1s).
            % Elegimos b, c, d porque la primera operacion siempre
            % comienza por dichas palabras iniciales
            x = dec2bin(b, 32) - '0';
            y = dec2bin(c, 32) - '0';
            z = dec2bin(d, 32) - '0';
            
            % Obtenemos f = mix of b, c, d.
            % ki = indice 0:15, del mensaje (k + ki).
            % sr = filas 1:4, de s(sr, :).
            if i <= 16
                % Ronda 1
                f = (x & y) | (~x & z);
                ki = i - 1;
                sr = 1;
            elseif i <= 32
                % Ronda 2
                f = (x & z) | (y & ~z);
                ki = mod(5 * i - 4, 16); % de 5 en 5 empezando en 1
                sr = 2;
            elseif i <= 48
                % Ronda 3
                f = xor(x, xor(y, z));
                ki = mod(3 * i + 2, 16); % de 3 en 3 empezando en 5
                sr = 3;
            else
                % Ronda 4
                f = xor(y, x | ~z);
                ki = mod(7 * i - 7, 16); % de 7 en 7 empezando en 0
                sr = 4;
            end
        
            % Convertir f DE VECTOR FILA DE BITS A ENTEROS DE 32-bit.
            f = uint32(f);   % pasar de logico a numero
            f = dec2bin(f)'; % pasar de numero a binario (cadena)
            f = bin2dec(f);  % pasar de binario al entero equivalente
            
            % HACEMOS LA ROTACIONES
            % Columna de matriz S
            sc = mod(i - 1, 4) + 1;
            suma = mod(a + f + mensaje(k + ki) + t(i), m);
            suma = dec2bin(suma, 32);
            % Desplazamiento a la izquierda segun el valor de S (en
            % binario)
            suma = circshift(suma, [0, s(sr, sc)]);
            % Recuperar el valor en decimal
            suma = bin2dec(suma); 
            
            % ACTUALIZAMOS a, b, c, d (rotacion).
            temp = d;
            d = c;
            c = b;
            b = mod(b + suma, m);
            a = temp;
            
        end
        
        % MODIFICAMOS EL HASH sumando las palabras iniciales
        fhash = mod(fhash + [a, b, c, d], m);
    
    end
    
    % CONVERTIMOS HASH DE ENTEROS DE 32 BITS, LITTLE ENDIAN, A BYTES
    % Convertir a big endian
    fhash = [fhash; fhash / (2^8); fhash / (2^16); fhash / (2^24)];
    % Convertir a bytes
    fhash = reshape(mod(floor(fhash), 256), 1, []);
    
    % CONVERTIMOS HASH A HEXADECIMAL
    fhash = reshape(dec2hex(fhash)', 1, []);
    
end