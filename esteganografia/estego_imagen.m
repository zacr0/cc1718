function estego_texto(ruta_imagen_base, ruta_imagen_secreta)
% Oculta un texto en una imagen en tonos de gris mediante el metodo LSB
% (less significant bit).
% ruta_imagen: ruta de la imagen en la que se oculta el mensaje.
% mensaje: texto a ocultar en la imagen.

    % TRATAMIENTO DE ENTRADA
    % Lectura de imagen original:
    imagen_original = imread(ruta_imagen_base);
    figure('Name', 'Imagen base');
    imshow(imagen_original);
    
    % Lectura de imagen secreta:
    imagen_secreta = imread(ruta_imagen_secreta);
    figure('Name', 'Imagen secreta');
    imshow(imagen_secreta);
    
    % Conversion de las imagenes a blanco y negro
    img_base_bn = rgb2gray(imagen_original);
    figure('Name', 'Imagen base en blanco y negro');
    imshow(img_base_bn);
    img_secreta_bn = rgb2gray(imagen_secreta);
    figure('Name', 'Imagen secreta en blanco y negro');
    imshow(img_secreta_bn);
    
    % Copia de la imagen, sera la estego-imagen
    estego_imagen = img_base_bn;
    
    % Conversion del texto a binario con longitud 8 bits
    img_bin = dec2bin(img_secreta_bn, 8);
    % Paso del mensaje a una cadena de una sola fila
    img_bin = reshape(img_bin', 1, []);
    
    % Comprobar si el mensaje cabe en la imagen (esto cambiaria en imagenes
    % a color, ya que tenemos el triple de espacio al tener 3 canales)
    tam_img_base_bn = size(img_base_bn, 1) * size(img_base_bn, 2);
    tam_img_secreta_bin = length(img_bin);
    if tam_img_secreta_bin > tam_img_base_bn
        error('Error: la imagen (longitud: %d bits) no cabe en la imagen (disponibles: %d bits).\n', tam_img_secreta_bin, tam_img_base_bn);
    end
    
    % Recorrido de la imagen, introduciendo un bit del mensaje en el bit
    % menos significativo de cada pixel cada vez
    % NOTA: se introduce el mensaje al comienzo de la imagen, por filas.
    k = 1;
    fin = 0;
    for i=1:size(img_base_bn, 1)
        for j=1:size(img_base_bn, 2)
            % Valor decimal del pixel actual
            nivel_de_gris_original = img_base_bn(i, j);

            if mod(nivel_de_gris_original, 2) == 0
                % Nivel de gris par
                if strcmp(img_bin(k), '1')
                    % Si el bit a ocultar es 1, sumar 1
                    nivel_de_gris_modificado = nivel_de_gris_original + 1;
                else
                    % Si el bit a ocultar es 0, no se hace nada
                    nivel_de_gris_modificado = nivel_de_gris_original;
                end

            else
                % Nivel de gris impar
                if strcmp(img_bin(k), '0')
                    % Si el bit a ocultar es 0, restar 1
                    nivel_de_gris_modificado = nivel_de_gris_original - 1;
                else
                    % Si el bit a ocultar es 1, no se hace nada
                    nivel_de_gris_modificado = nivel_de_gris_original;
                end
            end

            % Actualizar el valor en la estego-imagen
            estego_imagen(i,j) = nivel_de_gris_modificado;
            
            % Iterar hasta introducir el mensaje completo
            k = k + 1;
            if k > tam_img_secreta_bin
                fin = 1;
                break;
            end
        end
        % Iterar hasta introducir el mensaje completo
        if fin
            break;
        end
    end
    
    % Mostrar y almacenar estego-imagen
    figure('Name', 'Estego-imagen');
    imshow(estego_imagen);
    imwrite(estego_imagen, './img/estego_imagen.bmp');
end