function mensaje = recuperar_texto(ruta_estego_imagen)
% Recupera el mensaje oculto en una imagen mediante el metodo LSB (bit
% menos significativo)

    % TRATAMIENTO DE ENTRADA
    % Lectura de imagen:
    estego_imagen = imread(ruta_estego_imagen);
    
    % Recuperacion del mensaje original, deshaciendo las operaciones
    % realizadas mediante LSB
    % NOTA: la recuperacion se hace por filas.
    % IMPORTANTE: en un programa real se deben utilizar operaciones
    % matriciales, de lo contrario el rendimiento del programa sera PESIMO.
    msg_bin = '';
    for i=1:size(estego_imagen, 1)
        for j=1:size(estego_imagen,2)
            nivel_de_gris = estego_imagen(i, j);

            if mod(nivel_de_gris, 2) == 0
                % Si el nivel de gris es par, el bit oculto es 0
                msg_bin = strcat(msg_bin, '0');
            else
                % Si el nivel de gris es impar, el bit oculto es 1
                msg_bin = strcat(msg_bin, '1');
            end
        end
    end
    
    % Partir en bloques de 8 bits (recuperandolo por filas)
    msg_bin = reshape(msg_bin', 8, [])';
    
    % Convertir mensaje de vuelta a ASCII
    mensaje = char(bin2dec(msg_bin))';
    
    % Mostrar mensaje oculto
    fprintf('Mensaje: ''%s''.\n', mensaje);
    
end