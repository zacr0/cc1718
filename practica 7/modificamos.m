function [amplio, matriz_mod] = modificamos(matriz, texto, ini_filas, col)
% Dada una matriz, modifica el bit menos significativo de cada elemento en
% funcion de los bits que representan al mensaje introducido en texto.
% Mediante ini_filas se indica a partir de que fila se empieza a modificar
% y con col el numero de columnas de la matriz que se toman para modificar
% la matriz.
% Se obtiene como salida:
%   - matriz, la matriz modificada tras modificar el bit menos
%   significativo.
%   - amplio, que indica el numero de elementos que ha sido necesario
%   introducir para completar la ultima fila sobre la que se ha modificado.

    % TRATAMIENTO DE ENTRADA
    % texto debe ser una cadena
    if ~ischar(texto)
        error('Error: texto debe ser una cadena de caracteres.');
    end
    
    % matriz, ini_filas y col deben ser un numero
    if ~isnumeric(matriz) || ~isnumeric([ini_filas, col])
        error('Error: los parametros de entrada debe ser numericos.');
    end
    % matriz, ini_filas y col deben ser un entero
    if any(mod(matriz(:), 1) ~= 0) || any(mod([ini_filas col], 1) ~= 0)
        error('Error: los parametros de entrada deben ser un entero.');
    end
    % matriz, ini_filas y col deben ser positivos
    if any(matriz(:) <= 0) || any([ini_filas, col] < 0)
        error('Error: los parametros de entrada deben ser positivos.');
    end
    
    % Las dimensiones de insercion deben ser validas
    if size(matriz, 1) < ini_filas
        error('Error: el valor de ini_filas supera el numero de filas de la matriz.');
    end
    if size(matriz, 2) < col
        error('Error: el valor de col supera el numero de columnas de la matriz.');
    end

    
    % INTRODUCCION DE MENSAJE EN MATRIZ
    % Paso de mensaje a matriz binaria
    [amplio, matriz_bit] = textobit_col(texto, col);
    len_msg = size(matriz_bit, 1) * size(matriz_bit, 2) - amplio;
    
    % Introduccion de mensaje en el bit menos significativo de cada pixel
    matriz_mod = matriz;
    k = 1; % iterador para conocer en que momento se alcanza el relleno
    fin = 0; % controla la iteracion sobre la matriz al alcanzar relleno
    for i=1:size(matriz_bit, 1)
        for j=1:size(matriz_bit, 2)
            pixel = matriz_mod(ini_filas + i - 1, j);
            
            if mod(pixel, 2) == 0
                % Valor par
                if strcmp(matriz_bit(i, j), '1')
                    % Si queremos introducir un 1, sumar 1
                    pixel = pixel + 1;
                end
            else
                % Valor impar
                if strcmp(matriz_bit(i, j), '0')
                    % Si queremos introducir un 0, restar 1
                    pixel = pixel - 1;
                end
            end
            
            % Actualizar el valor de ese pixel
            matriz_mod(ini_filas + i - 1, j) = pixel;
            
            % Comprobar si se alcanza relleno
            k = k + 1;
            if amplio > 0 && k > len_msg
                % Detener si se ha alcanzado fin del mensaje
                fin = 1;
                break;
            end   
        end
        % Detener si se ha alcanzado fin del mensaje
        if fin
            break;
        end 
    end
end