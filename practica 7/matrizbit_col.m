function [amplio, matriz_bit] = matrizbit_col(matriz, col)
% Transforma una matriz numerica en su representacion equivalente en 8 bits
% y la almacena en una matriz matriz_bit de col columnas empleando el metodo
% del bit menos significativo.
% Se introduce relleno en la ultima fila, en caso de ser necesario. En
% caso de introducirse, el numero de 0 introducidos es indicado por amplio.

    % TRATAMIENTO DE ENTRADA
    % Las entradas deben ser numericas
    if any(~isnumeric(matriz)) || ~isnumeric(col)
        error('Error: los valores de entrada deben ser datos numericos.');
    end
    % Las entradas deben ser de tipo entero
    if any(mod(matriz(:), 1) ~= 0) || mod(col, 1) ~= 0
        error('Error: los valores de entrada deben ser de tipo entero.');
    end
    % Las entradas deben ser positivas
    if any(matriz(:) < 0) || col <= 0
        error('Error: los valores de entrada deben ser positivos.');
    end
    
    
    % TRANSFORMACION DE MATRIZ
    % Paso de matriz a vector fila de valores 0-255
    matriz_bit = reshape(matriz', 1, []);
    matriz_bit = mod(matriz_bit, 256);
    
    % Conversion de elementos a bit
    matriz_bit = dec2bin(matriz_bit, 8);
    matriz_bit = reshape(matriz_bit', 1, []);
    
    % Introduccion de relleno para completar la ultima fila de la matriz
    amplio = 0;
    while mod(length(matriz_bit), col) ~= 0
        % Rellenar con 0
        matriz_bit = strcat(matriz_bit, '0');
        amplio = amplio + 1;
    end
    
    % Conversion de texto a matriz
    matriz_bit = reshape(matriz_bit, col, [])';
end