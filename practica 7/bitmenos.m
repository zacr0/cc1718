function [numero, amplio, capa] = bitmenos(texto, foto, capa)
% Esconde un texto en la capa indicada de una foto mediante el método del
% bit menos significativo LSB.
% Se obtiene como salida una nueva foto de nombre 'fotomens.bmp', que se
% corresponde con la estego-imagen obtenida.
% Se obtiene tambien como salida la cantidad amplio de relleno introducido,
% junto con el numero de bits modificados.

    % TRATAMIENTO DE ENTRADA
    % La foto debe existir
    if ~exist(foto, 'file')
        error('Error: La ruta de foto no existe.');
    end
    
    % capa debe ser un numero
    if ~isnumeric(capa)
        error('Error: capa debe ser un dato numerico.');
    end
    % capa debe ser un entero
    if mod(capa, 1) ~= 0
        error('Error: capa debe ser un entero.');
    end
    % capa debe ser positivo
    if capa <= 0
        error('Error: capa debe ser positivo.');
    end
    
    
    % ALTERACION DE IMAGEN
    % Lectura de imagen y mensaje
    img = imread(foto);
    numero = length(texto) * 8;
    
    % Comprobacion de capa valida
    if capa > size(img, 3)
        error('Error: el valor de capa es mayor que el numero de capas disponibles.');
    end
    
    % Introduccion de mensaje en imagen en la primera fila y con tantas
    % columnas como ancho tiene la imagen
    [amplio, img(:,:, capa)] = modificamos(img(:, :, capa), texto, 1, size(img, 2));
    
    % Almacenamiento de estego-imagen obtenida
    imwrite(img, 'fotomens.bmp');
end