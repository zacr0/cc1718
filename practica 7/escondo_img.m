function [amplio, f_chica, c_chica] = escondo_img(fotochica, fotocobertura)
% Esconde una imagen fotochica (mensaje oculto) en una imagen de cobertura,
% fotocobertura.
% Se devuelve:
%   - amplio: cantidad de 0s introducidos como relleno en la ultima fila
%   donde se ha introducido informacion.
%   - f_chica: foto de cobertura modificada con el mensaje oculto
%   introducido.
%   - c_chica: tamaño de fotochica.

    % Lectura de imagenes
    if ~exist(fotochica, 'file')
        error('La imagen fotochica no existe.');
    end
    fotochica = imread(fotochica);
    % Tamaño de la imagen oculta
    c_chica = size(fotochica);
    
    if ~exist(fotocobertura, 'file')
        error('La imagen fotocobertura no existe.');
    end
    fotocobertura = imread(fotocobertura);
    
    % Introduccion de imagen oculta en imagen de cobertura (nota: se ha
    % introducido a partir de la primera fila).
    for i=1:size(fotocobertura,3)
        [amplio, f_chica(:,:,i)] = modificamos_img(fotocobertura(:,:,i), fotochica(:,:,i), 1);
    end
    
    % Almacenamiento de la imagen de cobertura modificada
    imwrite(f_chica, 'esconde.bmp');
end