function foto_chica = encuentro_img(c_chica, f_chica, fotogrande_mod)
% Extrae la imagen oculta en una imagen fotogrande_mod, sabiendo que tiene
% f_chica filas y c_chica columnas.
% Devuelve y muestra la foto oculta obtenida.
    
    % TRATAMIENTO DE ENTRADA
    % Las entradas deben ser numericas
    if any(~isnumeric([c_chica, f_chica]))
        error('Error: los valores de entrada deben ser datos numericos.');
    end
    % Las entradas deben ser de tipo entero
    if any(mod([c_chica, f_chica], 1) ~= 0)
        error('Error: los valores de entrada deben ser de tipo entero.');
    end
    % Las entradas deben ser positivas
    if any([c_chica, f_chica] <= 0)
        error('Error: los valores de entrada deben ser positivos.');
    end
    
    % EXTRACCION DE IMAGEN OCULTA
    % Lectura de imagen de entrada
    if ~exist(fotogrande_mod, 'file')
        error('La imagen fotogrande_mod no existe.');
    end
    fotogrande_mod = imread(fotogrande_mod);
    
    % Determinar espacio ocupado por la foto chica en la foto grande
    num_elem_por_capa = f_chica * c_chica * 8;
    fin_filas = ceil(num_elem_por_capa / size(fotogrande_mod, 2));
    admas = mod(num_elem_por_capa, size(fotogrande_mod, 2));
    
    % Recuperacion de imagen oculta
    % Inicializacion de foto_chica: es muy importante especificar el tipo
    % como entero, de lo contrario no se interpretara como una foto por el
    % entorno (ejemplo: imshow mostrara una imagen en blanco)
    foto_chica = zeros(f_chica, c_chica, 3, 'uint8');
    for i=1:size(fotogrande_mod, 3)
        % Extraccion de bit menos significativo de cada pixel de interes
        bits_capa = obtengo_bit(fotogrande_mod(:, :, i), 1, fin_filas, admas);
        
        % Recuperacion de valores de pixeles (por filas, en decimal)
        pixeles_capa = reshape(bits_capa, 8, [])';
        pixeles_capa = bin2dec(pixeles_capa(:,:));
        
        % Almacenamiento de capa recuperada
        foto_chica(:,:,i) = reshape(pixeles_capa, f_chica, c_chica)';
    end
    
    % Mostrar imagen oculta
    figure('Name', 'Imagen oculta');
    imshow(foto_chica);
    
end