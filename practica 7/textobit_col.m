function [amplio, texto_bit] = textobit_col(texto, col)
% Convierte un texto a su representacion equivalente en binario, la cual 
% almacenada en una matriz con tantas columnas como el parametro col
% indique. Dicha matriz queda rellenada por filas.
% Se obtiene como salida:
%   - La matriz de digitos binarios texto_bit.
%   - amplio, valor que indica el numero de elementos de relleno
%   introducidos para completar la ultima fila.

    % TRATAMIENTO DE ENTRADA
    % texto debe ser una cadena
    if ~ischar(texto)
        error('Error: texto debe ser una cadena de caracteres.');
    end
    
    % col debe ser un numero
    if ~isnumeric(col)
        error('Error: col debe ser un dato numerico.');
    end
    % col debe ser un entero
    if mod(col, 1) ~= 0
        error('Error: col debe ser un entero.');
    end
    % col debe ser positivo
    if col <= 0
        error('Error: col debe ser positivo.');
    end
    
    
    % CONVERSION DE TEXTO A MATRIZ DE DIGITOS BINARIOS
    % Conversion de texto a cadena de 8 bits de longitud
    texto_bit = dec2bin(texto, 8);
    texto_bit = reshape(texto_bit', 1, []);
    
    % Introduccion de relleno para completar la ultima fila de la matriz
    amplio = 0;
    while mod(length(texto_bit), col) ~= 0
        % Relleno con 0
        texto_bit = strcat(texto_bit, '0');
        amplio = amplio + 1;
    end
    
    % Conversion de texto a matriz
    texto_bit = reshape(texto_bit, col, [])';
end