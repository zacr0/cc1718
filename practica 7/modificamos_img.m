function [amplio, matriz_cob_modi] = modificamos_img(matriz_cob, matriz, ini_fila)
% Introduce en la matriz matriz_cob el contenido de matriz utilizando el
% metodo del bit menos significativo. La informacion se introducira desde
% la fila ini_fila en adelante.
% Devuelve 
%   - amplio, indicando la cantidad de 0s introducidos para rellenar
% la ultima fila.
%   - matriz_cob_modi, la matriz modificada tras introducir la
% informacion, si ha sido posible.

    % TRATAMIENTO DE ENTRADA
    % Las entradas deben ser numericas
    if any(~isnumeric(matriz_cob)) || any(~isnumeric(matriz)) || ~isnumeric(ini_fila)
        error('Error: los valores de entrada deben ser datos numericos.');
    end
    % Las entradas deben ser de tipo entero
    if any(mod(matriz_cob(:), 1) ~= 0) || any(mod(matriz(:), 1) ~= 0) || mod(ini_fila, 1) ~= 0
        error('Error: los valores de entrada deben ser de tipo entero.');
    end
    % Las entradas deben ser positivas
    if any(matriz(:) < 0) || ini_fila <= 0
        error('Error: los valores de entrada deben ser positivos.');
    end
    
    % La fila de insercion debe ser valida
    if size(matriz_cob, 1) < ini_fila
        error('Error: el valor de ini_fila supera el numero de filas de la matriz.');
    end
    
    
    % INTRODUCCION DE INFORMACION EN MATRIZ
    % Comprobar si la matriz cabe en el canal de rojos
    if numel(matriz) > numel(matriz_cob)
        error('Error: la matriz de cobertura no tiene el tamaño suficiente para almacenar matriz.');
    end
    if size(matriz, 2) > size(matriz_cob, 2)
        error('Error: la matriz de coberura no tiene el ancho suficiente para almacenar matriz.');
    end
    
    % Conversion a matriz binaria
    [amplio, matriz_bit] = matrizbit_col(matriz, size(matriz_cob, 2));
    %len_msg = numel(matriz_bit) - amplio;
    len_msg = numel(matriz_bit);
    
    % Introduccion de matriz en el bit menos significativo de cada pixel de
    % la matriz de cobertura
    matriz_cob_modi = matriz_cob;
    k = 1; % iterador para conocer en que momento se alcanza el relleno
    fin = 0; % controla la iteracion sobre la matriz al alcanzar relleno
    for i=1:size(matriz_bit, 1)
        for j=1:size(matriz_bit, 2)
            pixel = matriz_cob_modi(ini_fila + i - 1, j);
            
            if mod(pixel, 2) == 0
                % Valor par
                if strcmp(matriz_bit(i, j), '1')
                    % Si queremos introducir un 1, sumar 1
                    pixel = pixel + 1;
                end
            else
                % Valor impar
                if strcmp(matriz_bit(i, j), '0')
                    % Si queremos introducir un 0, restar 1
                    pixel = pixel - 1;
                end
            end
            
            % Actualizar el valor de ese pixel
            matriz_cob_modi(ini_fila + i - 1, j) = pixel;
            
            % Comprobar si se alcanza relleno
            k = k + 1;
            
            if k > len_msg
                % Detener si se ha alcanzado fin del mensaje
                fin = 1;
                break;
            end   
        end
        % Detener si se ha alcanzado fin del mensaje
        if fin
            break;
        end 
    end
end