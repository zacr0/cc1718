function mensaje = des_bitmenos(numero, fotomens, capa)
% Recupera el mensaje oculto en la capa de una imagen fotomens, deshaciendo 
% las operaciones correspondientes al método LSB.
% Se devuelve el mensaje recuperado.

    % TRATAMIENTO DE ENTRADA
    % La foto debe existir
    if ~exist(fotomens, 'file')
        error('Error: La ruta de fotomens no existe.');
    end
    
    % numero y capa deben ser un numero
    if any(~isnumeric([capa numero]))
        error('Error: numero y capa deben ser un dato numerico.');
    end
    % numero y capa deben ser un entero
    if any(mod([capa numero], 1) ~= 0)
        error('Error: numero y capa deben ser un entero.');
    end
    % numero y capa debe ser positivo
    if any([capa numero] <= 0)
        error('Error: numero y capa deben ser positivos.');
    end
    
    
    % RECUPERACION DE MENSAJE
    img = imread(fotomens);
    
    % Comprobacion de capa valida
    if capa > size(img, 3)
        error('Error: el valor de capa es mayor que el numero de capas disponibles.');
    end
    
    
    % Control mediante contador del tamaño del mensaje
    k = 1;
    fin = 0;
    % Recuperacion de informacion deshaciendo los cambios realizados en el
    % bit menos significativo
    mensaje = '';
    for i=1:size(img(:, :, capa), 1)
        for j=1:size(img(:, :, capa), 2)
            pixel = img(i, j, capa);
            
            if mod(pixel, 2) == 0
                % Si es par, el bit oculto es 0
                mensaje = strcat(mensaje, '0');
            else
                % Si es impar, el bit oculto es 1
                mensaje = strcat(mensaje, '1');
            end
            
            % Comprobar si se alcanza fin del mensaje
            k = k + 1;
            if k > numero
                % Detener si se ha alcanzado fin del mensaje
                fin = 1;
                break;
            end 
        end
        % Detener si se ha alcanzado fin del mensaje
        if fin
            break;
        end 
    end
    
    % Partir mensaje en bloques de 8 bits (recuperandolo por filas)
    mensaje = reshape(mensaje, 8, [])';
    
    % Convertir mensaje de vuelta a ASCII
    mensaje = char(bin2dec(mensaje))';
end