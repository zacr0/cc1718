function obten=obtengo_bit(matriz, ini_filas, fin_filas, admas)
% Recupera un vector de bits menos significativos de los pixeles
% contenidos en una matriz. Se extrae a partir de la fila ini_filas hasta
% la fila fin_filas. admas indica el numero de elementos que deben tomarse
% de esa ultima fila.

    % TRATAMIENTO DE ENTRADA
    % Las entradas deben ser numericas
    if any(~isnumeric(matriz(:))) || any(~isnumeric([ini_filas, fin_filas, admas]))
        error('Error: los valores de entrada deben ser datos numericos.');
    end
    % Las entradas deben ser de tipo entero
    if any(mod(matriz(:), 1) ~= 0) || any(mod([ini_filas, fin_filas, admas], 1) ~= 0)
        error('Error: los valores de entrada deben ser de tipo entero.');
    end
    % Las entradas deben ser positivas
    if any(matriz(:) < 0) || any([ini_filas, fin_filas, admas] < 0)
        error('Error: los valores de entrada deben ser positivos.');
    end
    % La fila de insercion debe ser valida
    if size(matriz, 1) < ini_filas || size(matriz, 1)  < fin_filas
        error('Error: los indices ini_filas y fin_filas deben mantenerse en los limites de la matriz.');
    end
    % Los indices deben ser validos
    if ini_filas > fin_filas
        error('Error: ini_filas no puede ser mayor que fin_filas.');
    end
    
    
    % EXTRACCION DE BITS MENOS SIGNIFICATIVOS
    % Vector de bits menos significativos
    obten = [];
    % Contador de elementos tomados de la ultima fila
    k = 0;
    fin = 0;
    for i=ini_filas:fin_filas
        for j=1:size(matriz,2)
            % Recoleccion de pixeles de interes
            obten = [obten, matriz(i,j)];
            
            % Extraer solo los admas valores de la ultima fila
            if i == fin_filas
                k = k+1;
            end
            if k == admas
                fin = 1;
                break;
            end
        end
        % Se han extraido todos los elementos, detener recorrido
        if fin
            break;
        end
    end
    
    % Extraccion de bit menos significativo de los pixeles extraidos
    obten = dec2bin(obten, 8);
    obten = obten(:, 8)';
end