function cifrado = cifroMochilaSuper(s, texto)
% Cifra un mensaje aplicando el metodo de la mochila.
% Para ello, se toma un mensaje a cifrar y se obtiene su representacion
% binaria a partir de los caracteres ASCII que lo componen.
% Acto seguido, se fragmentan en bloques del mismo tamaño de la mochila y
% se multiplican por el vector mochila, lo que resulta en un vector de
% numeros que representan el mensaje cifrado.
% La mochila s debe ser supercreciente, de lo contrario no se aplicara el
% metodo de cifrado.
    
    % CIFRADO CON MOCHILA SUPERCRECIENTE
    cifrado = [];
    if mochila(s)
        cifrado = cifr_mochila(s, texto);
    end
end