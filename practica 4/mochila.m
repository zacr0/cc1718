function valida=mochila(s)
% Determina si el vector s cumple las condiciones necesarias para tratarse
% de una mochila supercreciente.
% Una mochila supercreciente es aquella en la que el elemento n-esimo es
% estrictamente mayor que la suma de los elementos predecesores (a0, a1,
% ... an-1)
    valida = 1;
    
    % TRATAMIENTO DE ENTRADA
    % El vector s debe ser numerico
    if ~isnumeric(s)
        error('Error: s debe ser un vector de numeros.');
    end
    % El vector s debe contener enteros
    if any(mod(s(:),1) ~= 0)
        error('Error: Los coeficientes de s deben ser enteros.');
    end
    % Los elementos de s deben ser positivos
    if any(s(:) < 0)
        error('Error: Los coeficientes de s deben ser positivos.');
    end
    % Los elementos deben estar ordenados crecientemente
    if (~issorted(s))
        fprintf('Error: Los coeficientes de s deben estar ordenados ascendentemente.\n');
        valida = 0;
    end
    
    
    % COMPROBACION DE MOCHILA SUPERCRECIENTE
    if valida
        for i=2:length(s)
            % Cada elemento debe ser mayor o igual que la suma de los anteriores
            if (s(i) < sum(s(1:i-1)))
                valida = 0;
            end
        end
        if ~valida
            fprintf('La mochila no es supercreciente.\n');
        end
    end
end