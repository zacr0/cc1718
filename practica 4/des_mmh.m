function mensaje=des_mmh(s1, cifrado, mu, invw)
% Descifra un mensaje recibido, sabiendo que este se ha encriptado
% empleando el metodo de la mochila.
    
    % DESCIFRADO
    % Recuperamos el mensaje binario
    M = mod(invw * cifrado, mu);
    % Descifrado y recuperacion del texto original
    mensaje = des_mochila(s1, M);
    
end