function mochila_simple=cripto_shamir_zippel(cp, mu)
% Aplica la tecnica de criptoanalisis de Shamir y Zippel para romper un
% cifrado de tipo mochila.
% Dado una clave publica conocida (mochila dificil cp) y el valor del
% modulo, mu, obtenido de alguna manera, es posible obtener la mochila
% simple (clave privada) si se dan unas determinadas condiciones.
%
% NOTA: las condiciones fuertes del algoritmo original figuran comentadas
% en el codigo, puede descomentarse para observar las diferencias en los
% resultados obtenidos.

    % TRATAMIENTO DE ENTRADA
    % El vector cp debe ser numerico
    if ~isnumeric(cp)
        error('Error: cp debe ser un vector de numeros.');
    end
    % El vector cp debe contener enteros
    if any(mod(cp(:),1) ~= 0)
        error('Error: Los coeficientes de cp deben ser enteros.');
    end
    % Los elementos de cp deben ser positivos
    if any(cp(:) < 0)
        error('Error: Los coeficientes de cp deben ser positivos.');
    end
    
    % mu debe ser un numero
    if ~isnumeric(mu)
        error('Error: mu debe ser un dato numerico.');
    end
    % mu debe ser un entero
    if mod(mu,1) ~= 0
        error('Error: mu debe ser un entero.');
    end
    % mu debe ser positivo
    if mu < 0
        error('Error: mu debe ser positivo.');
    end
    
    
    % CRIPTOANALISIS
    % Busqueda de q usando los primeros valores de la clave publica
    [G, inv_s2, ~] = gcd(cp(2), mu);
    % Condicion fuerte: S2 debe tener inverso modulo mu
    %if G ~= 1
    %    error('Error: %d no tiene inverso modulo %d.\n', cp(2), mu);
    %end
    q = mod(cp(1) * inv_s2, mu);
            
    % Limite del rango a calcular (2^m+i) --> i = 1, 2, ...
    m = length(cp)+1;
    limite_rango = 0;
    
    % Busqueda de clave
    salir = false;
    while ~salir
        % Comienzo de analisis de tiempo de ejecución
        tic;
        
        % Calculo de los multiplos modulares de q en el rango [1, 2^(m+i)]
        rango = 2^(m+limite_rango);
        fprintf('Buscando en el rango [1, %d]...\n', rango);
        candidatos = zeros(1, rango+1);
        for i=1:length(candidatos)
            candidatos(i) = mod(i * q, mu);
        end
        
        % Ordenacion de candidatos de menor a mayor
        candidatos = sort(candidatos);
        
        % Busqueda de mochila supercreciente usando el candidato mas
        % pequeño
        for k=1:length(candidatos)
            % Busqueda de omega
            [G, inv_s1, ~] = gcd(candidatos(k), mu);
            % Condicion fuerte: S1 debe tener inverso modulo mu
            %if G == 1
            % S1' tiene inverso, se continua la busqueda
            % Calculo del inverso de omega
            omega = mod(cp(1) * inv_s1, mu);
            [~, inv_omega, ~] = gcd(omega, mu);

            % Calculo de mochila candidata (clave privada)
            mochila_simple = mod(cp * inv_omega, mu);
            % Comprobacion de mochila valida (es supercreciente)
            if mochila(mochila_simple)
                fprintf('Mochila simple encontrada.\n');
                salir = true;
                break;
            end
            %end
        end
        
        if ~salir
            % No se ha encontrado la clave con el rango actual, preguntar al
            % usuario si se continua buscando
            aumentar_rango = input('No se ha encontrado la clave. Para ampliar el rango, introduzca 1: ');
            if aumentar_rango == 1
                limite_rango = limite_rango + 1;
            else
                fprintf('Busqueda cancelada.\n');
                salir = true;
            end
        end
    end
    
    % Fin de analisis de tiempo de ejecución
    fprintf('Tiempo empleado en la busqueda: %f segundos.\n', toc);
end