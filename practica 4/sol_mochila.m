function [v, valida] = sol_mochila(s, obj)
% Dada una mochila s, comprueba si se puede cumplir el objetivo obj. 
% Una mochila cumple un objetivo cuando es posible igualar obj con la suma
% de algunos (o todos) los elementos que la componen.

    % TRATAMIENTO DE ENTRADA
    % La mochila debe ser supercreciente
    valida = mochila(s);
    
    % obj debe ser un numero
    if ~isnumeric(obj)
        error('Error: obj debe ser un dato numerico.');
    end
    % obj debe ser un entero
    if mod(obj,1) ~= 0
        error('Error: obj debe ser un entero.');
    end
    % obj debe ser positivo
    if obj < 0
        error('Error: obj debe ser positivo.');
    end
    
    
    % CALCULO DE SOLUCION DE MOCHILA
    v = zeros(1,length(s));
    
    % Recorrido por mochila en sentido inverso
    for i=length(s):-1:1
        if (s(i) <= obj)
            % El elemento forma parte de la solucion
            obj = obj - s(i);
            v(i) = 1;
        end
    end
    
    % Comprobar si se alcanza el objetivo
    if obj ~= 0
        fprintf('El objetivo no se alcanza con el algoritmo utilizado.\n');
        v = 0;
    else
        fprintf('Se alcanza el objetivo.\n');
    end
end