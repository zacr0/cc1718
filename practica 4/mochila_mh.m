function [cp, mu, invw] = mochila_mh(s1)
% Genera las claves publicas y privadas necesarias para transmitir mensajes
% cifrados mediante el metodo del cifrado con mochila trampa.
%
% La entrada s1 es una mochila que debe ser supercreciente, y se solicitara
% al usuario un valor mu que debe superar, al menos, el valor resultante de
% sumar todos los elementos de la mochila introducida.
%
% Como salida se obtendra el inverso del valor w encontrado para el mu
% introducido, del cual debe ser primo relativo y sin factores primos
% comunes con los elementos de s1; y la mochila cp resultante de multiplica
% los elementos de s1 por w.

    if mochila(s1)
        % La mochila es valida
       
        % Peticion de valor de mu al usuario
        mu_invalido = 1;
        minimo_aceptable = 2*s1(length(s1));
        while mu_invalido
            mu = input(sprintf('- Introduzca un valor de mu valido (mayor que %d): ', minimo_aceptable));
            
            % Comprobacion de validez de mu
            % mu debe ser un numero
            if ~isnumeric(mu)
                fprintf('Error: mu debe ser un dato numerico.\n');
            end
            % mu debe ser un entero
            if mod(mu,1) ~= 0
                fprintf('Error: mu debe ser un entero.\n');
            end
            % mu debe ser positivo
            if mu < 0
                fprintf('Error: mu debe ser positivo.\n');
            end
            % mu debe ser mayor que la suma de los elementos de la mochila
            if mu >= minimo_aceptable
                mu_invalido = 0;
            end
        end
        
        % Busqueda de valor de w valido
        for i=mu-1:-1:1
            if gcd(mu, i) == 1 && ~factorescomunes(i, s1)
                w = i;
                display(w);
                break;
            end
        end
        
        % Inversa de w
        [G, invw, V] = gcd(w, mu);
        invw = mod(invw, mu);
        
        % Calculo de mochila dificil o trampa
        cp = mod(w * s1, mu);
        
    end
end