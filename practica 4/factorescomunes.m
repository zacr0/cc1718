function factores_c = factorescomunes(w, s)
% Comprueba si un vector de enteros positivos s (mochila de cifrado) tiene
% factores primos comunes con un entero positivo w.

    % TRATAMIENTO DE ENTRADA
    % w debe ser un numero
    if ~isnumeric(w)
        error('Error: w debe ser un dato numerico.');
    end
    % w debe ser un entero
    if mod(w,1) ~= 0
        error('Error: w debe ser un entero.');
    end
    % w debe ser positivo
    if w < 0
        error('Error: w debe ser positivo.');
    end
    
    % s debe ser un vector fila
    if size(s, 1) > 1
        error('Error: s debe ser un vector fila.');
    end
    % El vector s debe ser numerico
    if ~isnumeric(s)
        error('Error: s debe ser un vector de numeros.');
    end
    % El vector s debe contener enteros
    if any(mod(s(:),1) ~= 0)
        error('Error: Los coeficientes de s deben ser enteros.');
    end
    % Los elementos de s deben ser positivos
    if any(s(:) < 0)
        error('Error: Los coeficientes de s deben ser positivos.');
    end
    
    
    % OBTENCION DE FACTORES COMUNES
    factores_c = 0;
    if any(gcd(w, s(:)) ~= 1)
        % Si algun elemento es divisible por w, hay factores comunes
        factores_c = 1;
    end
end