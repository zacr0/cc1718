function mensaje=des_mochila(s, cifrado)
% Descifra un mensaje encriptado mediante el metodo de la mochila.
    % TRATAMIENTO DE ENTRADA
    % Tanto s como cifrado deben ser vectores fila
    if size(s, 1) > 1
        error('Error: s debe ser un vector fila.');
    end
    if size(cifrado,1) > 1
        error('Error: cifrado debe ser un vector fila.');
    end
    
    
    % DESCIFRADO
    % Solucion de cada elemento de cifrado para obtener matriz binaria
    mat_textob2 = zeros(size(cifrado,2), size(s, 2));
    for i=1:size(cifrado,2)
        mat_textob2(i,:) = sol_mochila(s, cifrado(i));
    end
    
    % Conversion de matriz resultado a vector fila
    mat_textob2 = reshape(mat_textob2', 1, []);
    
    % Descarte de relleno, si existe
    if mod(length(mat_textob2), 8) ~= 0
        % Extraemos el vector con longitud mas cercana al maximo multiplo
        % de 8 posible
        mat_textob2 = mat_textob2(1, 1:8*floor(length(mat_textob2)/8));
    end
    
    % Agrupamiento en bloques de 8 bits
    mat_textob2 = reshape(mat_textob2, 8, [])';
    
    % Recuperacion de indices ASCII
    mat_textob2 = bin2dec(int2str(mat_textob2));
    
    % Obtencion de caracteres correspondientes
    mensaje = char(mat_textob2)';
end