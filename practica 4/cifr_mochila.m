function cifrado=cifr_mochila(s, texto)
% Cifra un mensaje empleando el metodo de la mochila. 
% Para ello, se toma un mensaje a cifrar y se obtiene su representacion
% binaria a partir de los caracteres ASCII que lo componen.
% Acto seguido, se fragmentan en bloques del mismo tamaño de la mochila y
% se multiplican por el vector mochila, lo que resulta en un vector de
% numeros que representan el mensaje cifrado.
    
    % TRATAMIENTO DE ENTRADA
    % El vector s debe ser numerico
    if ~isnumeric(s)
        error('Error: s debe ser un vector de numeros.');
    end
    % El vector s debe contener enteros
    if any(mod(s(:),1) ~= 0)
        error('Error: Los coeficientes de s deben ser enteros.');
    end
    % Los elementos de s deben ser positivos
    if any(s(:) < 0)
        error('Error: Los coeficientes de s deben ser positivos.');
    end
    
    % texto debe ser una cadena de caracteres
    if ~ischar(texto)
        error('Error: texto debe ser una cadena de caracteres.');
    end
    
    
    % CIFRADO CON MOCHILA
    % Paso de mensaje a vector de digitos en binario
    texto_numerico = dec2bin(texto, 8);
    texto_numerico = reshape(texto_numerico', 1, []);
    
    % Informacion para la fragmentacion en bloques (y posible relleno)
    long_bloque = length(s);
    long_msj = length(texto_numerico);
    n_bloques = ceil(long_msj / long_bloque);
    long_relleno = n_bloques * long_bloque;
    
    % Introduccion de relleno en bloques, si es necesario
    if long_msj ~= long_relleno
        for i=long_msj+1:long_relleno
            % Relleno con 1s
            texto_numerico = strcat(texto_numerico, '1');
        end
    end
    
    % Descomposicion en bloques de n_bloques x long_bloque
    texto_numerico = reshape(texto_numerico, long_bloque, n_bloques)';
    
    % Paso por la mochila
    cifrado = zeros(1,n_bloques);
    for i=1:n_bloques
        for j=1:long_bloque
            if texto_numerico(i,j) == '1'
                cifrado(1,i) = cifrado(1,i) + s(j);
            end
        end
    end
end